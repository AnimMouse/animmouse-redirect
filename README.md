# AnimMouse Redirect
Redirect .eu.org to .com on Google App Engine.

```
gcloud app deploy -v 1
gcloud config set project animmouse-website
gcloud app deploy dispatch.yaml
```

Uses HTTP 303 to redirect.